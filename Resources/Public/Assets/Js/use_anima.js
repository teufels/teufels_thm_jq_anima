var teufels_thm_jq_anima__interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {
    }  else {

        clearInterval(teufels_thm_jq_anima__interval);

        loadScript("/typo3conf/ext/teufels_thm_jq_anima/Resources/Public/Assets/Js/minimit-anima.min.js.gzip?v=1", function () {
            if (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development") {
                console.info('jQuery anima loaded');
            }

            // Initialize parameters
            var i_t2page_anima_delay_start = 1000;
            var i_t2page_anima_delay = 100;
            var s_t2page_anima_bezir = "0,.63,.41,.98";
            var s_t2page_anima_selector = ".anima";

            // Initialize function
            function t2page_anima_default (i_t2page_anima_delay_start, s_t2page_anima_bezir, s_t2page_anima_selector, i_t2page_anima_delay) {
                $('body').find(s_t2page_anima_selector).each(function (e) {
                    var t = $(this);
                    t.clearAnima().stopAnima().delayAnima(i_t2page_anima_delay_start + i_t2page_anima_delay * e).anima({y: 0, opacity: 1}, 800, s_t2page_anima_bezir).addClass('animated');
                });
            }

            // Run function
            $(window).load(function(){
                if (typeof t2page_anima_default === "function") {
                    if (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development") {
                        console.info("function 't2page_anima_default' present");
                    }
                    t2page_anima_default (i_t2page_anima_delay_start, s_t2page_anima_bezir, s_t2page_anima_selector, i_t2page_anima_delay);
                } else {
                    if (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development") {
                        console.info("function 't2page_anima_default' _not_ present");
                    }
                }
            });
            $(window).trigger('load');

        });
    }

}, 2000);

